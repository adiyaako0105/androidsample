package com.comax.israelcompanies.presentation.main

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.comax.israelcompanies.R
import com.comax.israelcompanies.data.CompaniesResponse
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.presentation.company.CompanyActivity
import com.comax.israelcompanies.presentation.company.CreateCompanyActivity
import com.comax.israelcompanies.presentation.fab_dialog.FabDialogDelegate
import com.comax.israelcompanies.presentation.fab_dialog.FabDialogFragment
import com.comax.israelcompanies.repo.CompaniesRepo
import com.comax.israelcompanies.utils.loadJSONFromAsset
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FabDialogDelegate {
    private lateinit var viewModel: MainViewModel
    private val disposeBag = CompositeDisposable()
    private val adapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Realm.init(this)
        buildUI()
        bind()
        viewModel.loadLocalJson()
    }

    private fun bind() {
        viewModel = MainViewModel(this)
        viewModel.start()

        viewModel.data.subscribe({data ->
            adapter.data = data
        }, {

        }).also { disposeBag.add(it)  }
    }


    private fun buildUI() {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        CompaniesRepo.loadJsonLocalCompanies()

        fab.setOnClickListener {
            FabDialogFragment.newInstance(this).show(supportFragmentManager, FabDialogFragment::class.java.simpleName)
        }
    }

    override fun dismissApp() {
        finish()
    }

    override fun createCompany() {
        startActivityForResult(Intent(this, CreateCompanyActivity::class.java), COMPANY_CREATION_REQUEST_CODE)
    }

    override fun clearCompaniesList() {
        viewModel.clearAllData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == COMPANY_CREATION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //Means Company created successfully
            viewModel.loadLocalJson()
        }

    }

    companion object {
        const val COMPANY_CREATION_REQUEST_CODE = 2134
    }
}
