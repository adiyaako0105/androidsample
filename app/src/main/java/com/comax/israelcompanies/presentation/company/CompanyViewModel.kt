package com.comax.israelcompanies.presentation.company

import android.net.Uri
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.repo.CompaniesRepo
import com.comax.israelcompanies.utils.isValid
import com.google.android.gms.maps.model.LatLng
import io.reactivex.subjects.BehaviorSubject
import java.net.URI

class CompanyViewModel {
    var name = BehaviorSubject.createDefault("")
    var address = BehaviorSubject.createDefault("")
    var city = BehaviorSubject.createDefault("")
    var imageUri = BehaviorSubject.create<Uri?>()
    var location = BehaviorSubject.create<LatLng?>()


    fun validFields() : Boolean {
        return name.value.isValid() && address.value.isValid() && city.value.isValid()
    }

    fun didMakeAnyChanges() : Boolean {
        return name.value!!.isNotEmpty() || address.value!!.isNotEmpty() || city.value!!.isNotEmpty() || (imageUri.value != null)
    }

    fun createCompany() {
        val company = Company()
        company.name = name.value
        company.imageUri = imageUri.value.toString()
        company.address = address.value.toString()
        company.city = city.value.toString()
        location.value?.let {
            company.longitude = it.longitude
            company.latitude = it.latitude
        }

        CompaniesRepo.saveCompanyToLocalDB(company)
    }
}