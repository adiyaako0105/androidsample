package com.comax.israelcompanies.presentation.company

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.comax.israelcompanies.utils.bind
import com.github.dhaval2404.imagepicker.ImagePicker
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_company.*
import com.google.android.material.snackbar.Snackbar
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory

interface CompanyActivityProtocol {
    fun bind()
}

open class CompanyActivity : AppCompatActivity(), OnMapReadyCallback, CompanyActivityProtocol {
    protected val viewModel = CompanyViewModel()
    protected var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.comax.israelcompanies.R.layout.activity_company)
        bind()
        val mapFragment = supportFragmentManager
            .findFragmentById(com.comax.israelcompanies.R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {

    }

    fun moveMapCamera(lat: Double?, lon: Double?) {
        if (lat == null || lon == null) {
            return
        }

        map?.moveCamera(CameraUpdateFactory.newLatLng(LatLng(lat, lon)))
        map?.animateCamera( CameraUpdateFactory.zoomTo( 8.0f))
    }

    override fun bind() {
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }


}
