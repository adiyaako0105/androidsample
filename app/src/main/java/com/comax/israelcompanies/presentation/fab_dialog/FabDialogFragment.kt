package com.comax.israelcompanies.presentation.fab_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.comax.israelcompanies.R
import com.comax.israelcompanies.repo.CompaniesRepo
import kotlinx.android.synthetic.main.dialog_fab.*
import java.lang.ref.WeakReference

interface FabDialogDelegate {
    fun dismissApp()
    fun createCompany()
    fun clearCompaniesList()
}

class FabDialogFragment : DialogFragment() {
    private var delegate: WeakReference<FabDialogDelegate>? = null

    companion object {
        fun newInstance(delegate: FabDialogDelegate): FabDialogFragment {
            val fragment = FabDialogFragment()
            fragment.setDelegate(delegate)
            return fragment
        }
    }

    fun setDelegate(delegate: FabDialogDelegate) {
        this.delegate = WeakReference(delegate)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_fab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clearCompaniesList.setOnClickListener {
            delegate?.get()?.clearCompaniesList()
            dismissDialog()
        }

        closeApplication.setOnClickListener {
            delegate?.get()?.dismissApp()
            dismissDialog()
        }

        createCompany.setOnClickListener {
            delegate?.get()?.createCompany()
            dismissDialog()
        }
    }

    private fun dismissDialog() {
        dialog?.dismiss()
    }

}