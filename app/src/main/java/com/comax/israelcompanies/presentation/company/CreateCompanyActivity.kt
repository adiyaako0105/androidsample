package com.comax.israelcompanies.presentation.company

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.comax.israelcompanies.R
import com.comax.israelcompanies.utils.bind
import com.comax.israelcompanies.utils.hideKeyboard
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_company.*

class CreateCompanyActivity: CompanyActivity() {
    private val disposeBag = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar.title = getString(R.string.create_company_title)
    }


    override fun onMapReady(p0: GoogleMap?) {
        super.onMapReady(p0)
        map = p0
        map?.let {
            it.setMaxZoomPreference(6f)
            moveMapCamera(32.0853, 34.7818)
            it.animateCamera( CameraUpdateFactory.zoomTo( 8.0f))
            it.setOnMapLongClickListener { latLng ->
                it.clear()
                viewModel.location.onNext(latLng)
                map?.addMarker(
                    MarkerOptions()
                        .position(latLng)
                )
            }
        } ?: run {
            Log.e("Error", "GMS is null")
        }

    }

    override fun bind() {
        super.bind()
        listenToPickImageEvents()
        companyNameEditText.bind().subscribe { newValue ->
            viewModel.name.onNext(newValue)
        }.also { disposeBag.add(it) }

        companyAddressEditText.bind().subscribe { newValue ->
            viewModel.address.onNext(newValue)
        }.also { disposeBag.add(it) }

        companyCityEditText.bind().subscribe { newValue ->
            viewModel.city.onNext(newValue)
        }.also { disposeBag.add(it) }

        saveCompany.setOnClickListener {
            if (viewModel.validFields()) {
                viewModel.createCompany()
                setResult(Activity.RESULT_OK, Intent())
                finish()
            } else {
                Toast.makeText(this, getString(com.comax.israelcompanies.R.string.please_fill_in_all_the_field), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun displayWarningSnackbar() {
        val snackBar = Snackbar.make(main, getString(com.comax.israelcompanies.R.string.are_you_sure), Snackbar.LENGTH_LONG)
            .setAction(getString(com.comax.israelcompanies.R.string.dismiss)) {
                super.onBackPressed()
            }
        snackBar.show()
    }

    override fun onBackPressed() {
        hideKeyboard()
        if (viewModel.didMakeAnyChanges()) {
            displayWarningSnackbar()

        } else {
            super.onBackPressed()
        }
    }

    private fun listenToPickImageEvents() {
        companyImageImageView.setOnClickListener {
            ImagePicker.with(this)
                .crop(1f, 1f)
                .compress(1024)
                .maxResultSize(1080, 1080)
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        data?.data?.let { fileUri ->
                            companyImageImageView.setImageURI(fileUri)
                            viewModel.imageUri.onNext(fileUri)
                        }
                    } else if (resultCode == ImagePicker.RESULT_ERROR) {
                        Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}