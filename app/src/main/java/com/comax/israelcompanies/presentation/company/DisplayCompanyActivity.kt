package com.comax.israelcompanies.presentation.company

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.comax.israelcompanies.R
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.repo.CompaniesRepo
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_company.*

class DisplayCompanyActivity: CompanyActivity() {
    companion object {
        const val COMPANY_ID: String = "COMPANY_ID"
    }
    var companyId: String? = null
    var company: Company? = null
    set(value) {
        field = value
        moveMapCamera(value?.latitude, value?.longitude)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        companyId = intent.getStringExtra(COMPANY_ID)
        saveCompany.visibility = View.GONE
        bind()
    }

    override fun bind() {
        super.bind()
        companyId?.let {id ->
            CompaniesRepo.companyById(id)?.let {company ->
                this.company = company
                companyNameEditText.setText(company.name)
                companyAddressEditText.setText(company.address)
                companyCityEditText.setText(company.city)

                companyNameEditText.isEnabled = false
                companyAddressEditText.isEnabled = false
                companyCityEditText.isEnabled = false

                company.imageUri?.let { image ->
                    companyImageImageView.setImageURI(Uri.parse(image))
                } ?: run {
                    Glide.with(this)
                        .load(company.imageUrl)
                        .apply(
                            RequestOptions()
                                .error(R.drawable.logo_placeholder)
                        ).into(companyImageImageView)
                }

            }
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        super.onMapReady(p0)
        moveMapCamera(company?.latitude, company?.longitude)

    }
}
