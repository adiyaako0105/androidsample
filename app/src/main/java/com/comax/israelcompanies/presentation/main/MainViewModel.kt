package com.comax.israelcompanies.presentation.main

import android.content.Context
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.repo.CompaniesRepo
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject

class MainViewModel(var context: Context)  {
    var data = BehaviorSubject.createDefault(ArrayList<Company>())
    private set
    private val disposeBag = CompositeDisposable()

    init {
    }

    fun start() {
        CompaniesRepo.fetchDataOnInit(context)

    }

    fun loadLocalJson() {
        CompaniesRepo.loadJsonLocalCompanies().subscribe( { data ->
            this.data.onNext(data)
        },  {

        }).also { disposeBag.add(it) }
    }

    fun clearAllData() {
        CompaniesRepo.shouldFetchLocalJson = false
        data.onNext(ArrayList())
        CompaniesRepo.clearAllLocalDBCompanies()
    }

}