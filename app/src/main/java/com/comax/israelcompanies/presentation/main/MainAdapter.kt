package com.comax.israelcompanies.presentation.main

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.comax.israelcompanies.R
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.presentation.company.DisplayCompanyActivity
import com.comax.israelcompanies.repo.CompaniesRepo
import java.lang.ref.WeakReference

class MainAdapter : RecyclerView.Adapter<MainAdapter.CompaniesViewHolder>() {

    var data = ArrayList<Company>()
        set(value) {

            value.sortByDescending { it.createdAt }
            value.reverse()
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CompaniesViewHolder =
        CompaniesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.cell_company,
                parent,
                false
            )
        )

    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: CompaniesViewHolder, position: Int) {
        val item = data[position]
        holder.cellAddress.text = item.address
        holder.cellTitle.text = item.name
        Glide.with(holder.itemView.context).load(com.comax.israelcompanies.R.drawable.logo_placeholder).into(holder.cellImage)
        item.imageUri?.let { image ->
            holder.cellImage.setImageURI(Uri.parse(image))
        } ?: run {
            Glide.with(holder.itemView.context)
                .load(item.imageUrl)
                .apply(
                    RequestOptions()
                        .error(R.drawable.logo_placeholder)
                )
                .into(holder.cellImage)
        }

        holder.cellDeleteAction.setOnClickListener {
            data.indexOfFirst { it.id == item.id }?.let {
                data.removeAt(it)
                notifyItemRemoved(it)
            }

            CompaniesRepo.clearCompany(item.id)
        }

        holder.itemView.setOnClickListener {
            Log.i("[AY]", item.id.toString())

            var context = holder.itemView.context
            val intent = Intent(context, DisplayCompanyActivity::class.java)
            intent.putExtra(DisplayCompanyActivity.COMPANY_ID, item.id)
            context.startActivity(intent)
        }


    }

    inner class CompaniesViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val cellImage: ImageView = item.findViewById(R.id.cellcompanyImage)
        val cellTitle: TextView = item.findViewById(R.id.cellCompanyName)
        val cellAddress: TextView = item.findViewById(R.id.cellCompanyAddress)
        val cellDeleteAction: ImageView = item.findViewById(R.id.cellCompanyDeleteAction)
    }
}