package com.comax.israelcompanies.repo

import android.content.Context
import android.util.Log
import com.comax.israelcompanies.data.CompaniesResponse
import com.comax.israelcompanies.data.Company
import com.comax.israelcompanies.utils.loadJSONFromAsset
import com.google.gson.Gson
import io.reactivex.Observable
import io.realm.Realm
import io.realm.kotlin.delete


object CompaniesRepo {
    var shouldFetchLocalJson = true
    private var realm: Realm? = null
    get() {
        return Realm.getDefaultInstance()
    }

    fun fetchDataOnInit(context: Context) {
        clearAllLocalDBCompanies()
        context.loadJSONFromAsset("companies.json")?.let {
            val data = Gson().fromJson(it, CompaniesResponse::class.java)
            data.companies.forEach { company ->
                saveCompanyToLocalDB(company)
            }
        } ?: run {
            Log.e("Error", "Missing json file")
        }
    }

    fun loadJsonLocalCompanies() : Observable<ArrayList<Company>> {
        return Observable.create<ArrayList<Company>> { emitter ->
            val dataList = ArrayList<Company>()
            dataList.addAll(dataList.size, fetchLocalDBCompanies())
            emitter.onNext(dataList)
            emitter.onComplete()
            return@create
        }
    }

    fun saveCompanyToLocalDB(company: Company) {
        realm?.let { realm ->
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(company)
            realm.commitTransaction()
        }
    }


    private fun fetchLocalDBCompanies() : ArrayList<Company> {
        realm?.let { realm ->
            val companies = realm.where(Company::class.java).findAll()
            return ArrayList(companies)
        } ?: kotlin.run {
            return ArrayList()
        }
    }

    fun clearAllLocalDBCompanies() {
        realm?.let { realm ->
            realm.beginTransaction()
            val results = realm.where(Company::class.java).findAll()
            results.deleteAllFromRealm()
            realm.commitTransaction()
        }
    }

    fun clearCompany(id: String) {
        realm?.let { realm ->
            realm.beginTransaction()
            companyById(id)?.deleteFromRealm()
            realm.commitTransaction()
        }
    }

    fun companyById(id: String) : Company? {
        return realm?.where(Company::class.java)?.findAll()?.first { it.id == id }
    }

}