package com.comax.israelcompanies.data

import android.net.Uri
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Company: RealmObject() {
    @PrimaryKey
    var id: String = randomId()
    var name: String? = null
    var city: String? = null
    var address: String? = null
    var longitude: Double? = null
    var latitude: Double? = null
    var imageUrl: String? = null
    var imageUri: String? = null
    var createdAt: Long = Date().time

    fun randomId() : String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"
        return (1..5)
            .map { allowedChars.random() }
            .joinToString("")
    }
}

