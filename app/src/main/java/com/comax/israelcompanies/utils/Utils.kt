package com.comax.israelcompanies.utils

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import io.reactivex.Observable
import java.io.IOException
import java.nio.charset.Charset
import java.util.*


fun Context.loadJSONFromAsset(fileName: String = "companies.json"): String? {
    var json: String? = null
    try {
        val `is` = assets.open(fileName)
        val size = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()
        json = String(buffer,  Charsets.UTF_8)

    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }

    return json
}

fun EditText.bind() : Observable<String> {
    return Observable.create<String> { emitter ->
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                emitter.onNext(s.toString())
            }
        })

    }
}

fun String?.isValid() : Boolean {
    this?.let {
        return this.trim().count() > 0
    } ?: run {
        return false
    }
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}